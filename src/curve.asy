import graph;
import animate;
usepackage("amsmath");
usepackage("amsfonts");
settings.outformat = "gif";
unitsize(1cm);
//size(0,20cm);
//settings.tex="pdflatex";
//settings.outformat="pdf";
size(0,400);
//defaultpen(0.5);
guide make_guide(pair[] pts, real straightness) {
  guide g;
  int n = pts.length; 
  real s = straightness;
  pair A = pts[0];
  pair B = pts[n-1];
  pair ABn = (B-A)/(n-1);
  for (int i=0; i<n; ++i) {
    pair pt = (1-s)*pts[i] + s*(A + ABn*i);
    g = g..pt;
  }
  return g;
}

struct CartesianGrid {
  pair at;
  real s;
  real L;
  int  N;
  void operator init(pair at, real L, int N) {
    this.s = 0.0;
    this.at = at; 
    this.L = L;
    this.N = N;
  }
  void draw(real s) {
    pair dx = (this.L/(N-1), 0);
    pair dy = (0, this.L/(N-1));
    for (int i=0; i<N; ++i){
      pair x1 = at+i*dx;
      pair x2 = at+i*dx + (0,L)*s;
      pair y1 = at+i*dy;
      pair y2 = at+i*dy + (L,0)*s;
      draw(x1--x2);
      draw(y1--y2);
    }
  }
  void doanimate(animation a, int N, real s1, real s2) {
    real ds = (s2-s1)/(N-1);
    for (int i=0; i<N; ++i) {
       real s = s1 + i*ds;
       save();
       this.draw(s);
       a.add();
       restore();
    }
    this.draw(s2);
    a.add();
  }
}
struct CoordSys {
  pair at;
  real s;
  real L;
  string xlabel;
  string ylabel;
  void operator init(pair at, real L) {
    this.s = 0.0;
    this.at = at; 
    this.L = L;
  }
  void draw(real s) {
    Label Lx= Label("$\xi$", position=EndPoint);
    Label Ly= Label("$\eta$", position=EndPoint);
    pair x = at + (L*s, 0);
    pair y = at + (0, L*s);
    draw(at--x, arrow=Arrow(), L=Lx);
    draw(at--y, arrow=Arrow(), L=Ly);
  }
  void doanimate(animation a, int N, real s1, real s2) {
    real ds = (s2-s1)/(N-1);
    for (int i=0; i<N; ++i) {
       real s = s1 + i*ds;
       save();
       this.draw(s);
       a.add();
       restore();
    }
    this.draw(s2);
    a.add();
  }
}

struct Curve {
  pair[] pts;
  guide p;
  real s;
  void operator init(pair a, pair b, pair c) {
    this.pts[0]=a;
    this.pts[1]=b;
    this.pts[2]=c;
    this.s = 0.0;
    this.p = make_guide(this.pts, this.s); 
  }
  void reset() {
    this.p = make_guide(this.pts, 0.0); 
  }
  void update(real s) {
    if (abs(this.s-s) > 1e-5) {
      this.p = make_guide(this.pts, s);
    } 
  }
  void draw(real tm, real s) { 
    this.update(s);
    draw(subpath(this.p,0,reltime(this.p,tm)));
  }
  void dorotate(real angle, pair center) {
    this.update(this.s);
    this.p = rotate(angle, center)*this.p;
  }
  void doshift(pair z) {
    this.update(this.s);
    this.p = shift(z)*this.p;
  }
  pair point(real tm, int orient=1) {
    if ( orient > 0) {
      return point(this.p, reltime(this.p, tm));
    } else {
      guide rp = reverse(this.p);
      return point(rp, reltime(rp, tm));
    }
  }
}

struct TransfiniteGrid {
  Curve[] sides;
  int Ngl;
  int res;
  real s;
  guide p;
  real y;
  void operator init(Curve[] sides, int Ngl, int res) {
    this.sides = sides;
    this.Ngl = Ngl;
    this.res = res;
    this.y = 0.0;
  }
  pair point(real u, real v) {
     pair c1 = this.sides[0].point(u);
     pair c2 = this.sides[1].point(v);
     pair c3 = this.sides[2].point(u,-1);
     pair c4 = this.sides[3].point(v,-1);
     pair p1 = this.sides[0].point(0);
     pair p2 = this.sides[1].point(0);
     pair p3 = this.sides[2].point(0);
     pair p4 = this.sides[3].point(0);

     pair pt = (1-v)*c1 + v*c3 + (1-u)*c2+u*c4 - ((1-u)*(1-v)*p1 
                + u*v*p3 + u*(1-v)*p2 + (1-u)*v*p4);
  
     return pt;
  }
  void setguide(real y) {
    pair [] pt;
    real du = 1.0/res;
    guide p;
    for (int i=0; i<this.res; ++i) {
      real u = du*i;
      real v = y;
      p = p..(this.point(u,v));
    }
    this.p = p;
    this.y = y;
  } 
  void draw() { 
    draw(this.p, red);
  }
  void dorotate(real angle, pair center) {
    this.p = rotate(angle, center)*this.p;
  }
  void doshift(pair z) {
    this.p = shift(z)*this.p;
  }
}

struct Patch {
  Curve[] sides;
  guide p;
  real s;
  void operator init(Curve[] sides) {
    this.sides = sides;
    s = 0.0;
      guide pl;
      for (int i=0; i<this.sides.length; ++i) {
         pl = pl--make_guide(this.sides[i].pts, s);
      }
      pl = pl--cycle;
      this.p = pl;
  }
  void setguide(real s){
      guide pl;
      for (int i=0; i<this.sides.length; ++i) {
         pl = pl--make_guide(this.sides[i].pts, s);
      }
      pl = pl--cycle;
      this.p = pl;
      this.s = s;
  }
  void reset() {
    this.setguide(0);
  }
  void update(real s) {
    if (abs(this.s -s) > 1e-5) {
      this.setguide(s);
    }
  }
  void draw(real s) { 
    update(s);
    fill(this.p,grey);
  }
  void draw_outline(real s) {
    update(s);
    draw(this.p);
  }
  void dorotate(real angle, pair center) {
    this.update(this.s);
    this.p = rotate(angle, center)*this.p;
  }
  void doshift(pair z) {
    this.update(this.s);
    this.p = shift(z)*this.p;
  }
  void draw_along(path p, real tm, real s, int what) {
    real t = reltime(p,tm);
    pair pt = point(p, t);
    real a = degrees(dir(p, t))-180;
    this.update(s);
    this.doshift(pt);
    this.dorotate(a, pt);
    if (what == 0) this.draw_outline(s);
    else this.draw(s);
  }
}
void pauseAnim(animation A, int N) {
  for (int i=0; i<10; ++i) {
    A.add();
  }
}

Curve c1 = Curve((0,0), (1,0.5), (2,0.0));
Curve c2 = Curve((2,0), (2.5,1), (2, 2));
Curve c3 = Curve((2,2), (1.8,2.1), (0, 2));
Curve c4 = Curve((0,2), (0.2,1), (0, 0));

Curve blend(Curve a, Curve b, real s) {
  Curve c = Curve((0,0), (0,0), (0,0));
  int N = a.pts.length;
  for (int i=0; i<a.pts.length; ++i) {
     c.pts[i] = (1-s)*a.pts[i] + b.pts[N-i-1]*s;
  }
  c.reset();
  return c;
}

animation A;
Curve[] pb = {c1, c2, c3, c4};

Curve ox1 = blend(c1, c3, 0.2);

pair start = (6,3);
path traj = start{(-1,-1)}..(2,0.2)..(0,0){left};
real a = degrees(dir(traj, 0))-180;
//c.draw(1,0);
TransfiniteGrid tg = TransfiniteGrid(pb, 4, 3);
tg.setguide(0.0);
tg.dorotate(a, (0,0));
tg.doshift(start);
Patch blob = Patch(pb);
ox1.dorotate(a, (0,0));
ox1.doshift(start);
blob.dorotate(a, (0,0));
blob.doshift(start);
blob.draw(0);
ox1.draw(1.0, 0.0);
tg.draw();
blob.reset();
draw(traj);
A.add();
int N = 100;
real dt = 1.0/N;
for (int i=0;i<N; ++i){
save();
//blob.draw_outline(dt*i);
blob.draw_along(traj, dt*i, dt*i, 0);
A.add();
restore();
}
blob.draw_outline(1);

CoordSys ksieta = CoordSys((0,0),3);
ksieta.doanimate(A,10, 2.0/3.0,1.0);
pauseAnim(A, 10);
CartesianGrid grid = CartesianGrid((0,0),2.0,8);
grid.doanimate(A,20,0,1);
pauseAnim(A, 20);
//A.movie(BBox(0.25cm));
A.movie(BBox(0.25cm),delay=50, loops=0, options="-density 150x150");

