import blob;
import animate;
unitsize(1cm);


TwinBlob b = TwinBlob(scale=5);
b.setTwist(new real[] {0,0,-40,0,30,0});
b.setAngle(new real[] {180,180,180,180});
b.moveCorner(4,(0,-1));
b.setPartDisplacement(1, (1, 0));
b.draw();
b.drawXiLine(0, 0.3, black+2);
b.blobs[0].drawOutline();
//b.drawGrid(index=0, nx=5, ny=5);
b.drawGrid(index=1, nx=5, ny=8, p=red);
//b.drawLoad(0.7, 0.9);
//guide g = b.getOutlineOffset(0.0, 1.0, distance=0.5);
//draw(g, p=red+2);
