import blob;
unitsize(1cm);

pair[] corners  = {(0,0), (5,1), (6,6), (0,6)};
real[] corner_angles = {180,180,180,180};
real[] corner_twist = {0,0,0,0};
real[] corner_factor = {1.6,1,1.6,1.6};
Blob b = Blob(corners, corner_angles, corner_twist, corner_factor);
Blob c = Blob(corners);
b.draw();
c.draw(blue);
c.drawArrows();
c.drawOutline(black+3);
