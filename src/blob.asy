import graph;
import animate;
import patterns;
unitsize(1cm);

add("hatch", hatch(H=2mm));

path[] getNormals(path pfrom, real distance, int density) {
  path[] normals;
  real alpha = 90;
  real dt=1.0/(density-1);
  for (int i=0; i<density; ++i) {
     real rtime = i*dt;
     pair spA = relpoint(pfrom, rtime);
     pair normal = unit(rotate(alpha)*dir(pfrom, reltime(pfrom, rtime)));
     pair spB = spA + distance*normal;
    normals.append(spA--spB);
  }
  return normals;
}

struct CartesianGrid {
  pair at;
  real s;
  real L;
  int  N;
  void operator init(pair at, real L, int N) {
    this.s = 0.0;
    this.at = at; 
    this.L = L;
    this.N = N;
  }
  void draw(real s) {
    pair dx = (this.L/(N-1), 0);
    pair dy = (0, this.L/(N-1));
    for (int i=0; i<N; ++i){
      pair x1 = at+i*dx;
      pair x2 = at+i*dx + (0,L)*s;
      pair y1 = at+i*dy;
      pair y2 = at+i*dy + (L,0)*s;
      draw(x1--x2);
      draw(y1--y2);
    }
  }
  void doanimate(animation a, int N, real s1, real s2) {
    real ds = (s2-s1)/(N-1);
    for (int i=0; i<N; ++i) {
       real s = s1 + i*ds;
       save();
       this.draw(s);
       a.add();
       restore();
    }
    this.draw(s2);
    a.add();
  }
}
struct CoordSys {
  pair at;
  real s;
  real L;
  string xlabel;
  string ylabel;
  void operator init(pair at, real L) {
    this.s = 0.0;
    this.at = at; 
    this.L = L;
  }
  void draw(real s) {
    Label Lx= Label("$\xi$", position=EndPoint);
    Label Ly= Label("$\eta$", position=EndPoint);
    pair x = at + (L*s, 0);
    pair y = at + (0, L*s);
    draw(at--x, arrow=Arrow(), L=Lx);
    draw(at--y, arrow=Arrow(), L=Ly);
  }
  void doanimate(animation a, int N, real s1, real s2) {
    real ds = (s2-s1)/(N-1);
    for (int i=0; i<N; ++i) {
       real s = s1 + i*ds;
       save();
       this.draw(s);
       a.add();
       restore();
    }
    this.draw(s2);
    a.add();
  }
}

import graph;
size(200,IgnoreAspect);
pair nvec(pair a) { 
  if (abs(a)<100*realMin) {
     return (0,0);
  } else {
     return unit((a.y,-a.x));
  }
}

path offset(path p, real dist=1) {
  pair off(real t) {
    real l=arclength(p); 
    return point(p,t)+dist*nvec(dir(p,t));
  };
  return graph(off,0,arclength(p), n=200, join=operator ..);
};

struct CornerArrows {
  path id; 
  path od;
  path pd;
  void draw() {
    draw(this.id, p=red, arrow=Arrow);
    draw(this.od, p=green, arrow=Arrow);
    draw(this.pd, p=blue, arrow=Arrow);
  }
}

struct Blob {
  CornerArrows[] corner_arrow = new CornerArrows[4];
  guide outline;
  real[] corner_angles;
  real[] corner_twist = {0,0,0,0};
  real[] side_factor = {1,1,1,1};
  pair[] corner_dirs = {(0,0),(0,0),(0,0),(0,0)};
  pair[] corners;
  path[] sides = new path[4];
  void operator init(pair[] corners = new pair[] {(0,0),(1,0),(1,1), (0,1)},
                     real[] corner_angles = new real[] {90,90,90,90},
                     real[] corner_twist = new real[] {0,0,0,0},
                     real[] side_factor=new real[] {1,1,1,1}, real scale=1.0) {
    int[] e = {2,3,0,1};
    int[] n = {1,2,3,0};
    this.corners = corners;
    for (int i=0; i<4; ++i) {
      this.corners[i] *= scale;
    }
    for (int i=0; i<4; ++i) {
      this.corner_dirs[i] = unit(this.corners[i]-this.corners[e[i]]);
      this.corner_arrow[i] = new CornerArrows;
    }
    this.corner_angles = corner_angles;
    this.corner_twist = corner_twist; 
    this.side_factor = side_factor;
  }
  pair getPDir(int at) {
      pair pdir = rotate(this.corner_twist[at])*this.corner_dirs[at];
      return pdir;
  }
  pair getSidePoint(int side, real t) {
     return relpoint(this.sides[side], t);
  }
  pair getSideNormal(int side, real t) {
     return rotate(-90)*dir(this.sides[side], reltime(this.sides[side], t));
  }
  void update() {
    int[] n = {1,2,3,0};
    int[] p = {3,0,1,2};
    for(int i=0; i<4; ++i) {
      pair pdir = rotate(this.corner_twist[i])*this.corner_dirs[i];
      pair ndir = rotate(this.corner_twist[n[i]])*this.corner_dirs[n[i]];
      pair idir = rotate(-this.corner_angles[i]/2)*(-pdir);
      pair odir = rotate(this.corner_angles[n[i]]/2)*(ndir);
      this.sides[i] = this.corners[i]{idir}..tension this.side_factor[i]..{odir}this.corners[n[i]];
      this.corner_arrow[n[i]].od = this.corners[n[i]]--(this.corners[n[i]]+odir);
      this.corner_arrow[i].id = this.corners[i]--(this.corners[i]-idir);
      this.corner_arrow[i].pd = this.corners[i]--(this.corners[i]+pdir);
    }
    this.outline = this.sides[0]--this.sides[1]--this.sides[2]--this.sides[3]--cycle;
  }
  void draw(pen p=gray) {
     this.update();
     fill(this.outline, p);
  }
  void drawArrows() {
     for(int i=0; i<4; ++i) {
       this.corner_arrow[i].draw();
     }
  }
  void drawOutline(pen p=black) {
    draw(this.outline, p=p);
  }

  void move(pair displacement) {
    for (int i=0; i<4; ++i) {
      this.corners[i] += displacement;
    }
  }

  pair gridPoint(real u, real v) {
     pair c1 = relpoint(this.sides[0],u);
     pair c2 = relpoint(this.sides[3],1-v);
     pair c3 = relpoint(this.sides[2],1-u);
     pair c4 = relpoint(this.sides[1],v);
     pair p1 = relpoint(this.sides[0],0);
     pair p2 = relpoint(this.sides[1],0);
     pair p3 = relpoint(this.sides[2],0);
     pair p4 = relpoint(this.sides[3],0);

     pair pt = (1-v)*c1 + v*c3 + (1-u)*c2 + u*c4 - ((1-u)*(1-v)*p1 
                + u*v*p3 + u*(1-v)*p2 + (1-u)*v*p4);
  
     return pt;
  }

  void drawGridLine(int dir, real x, pen p=black, int resolution=10) {
    real dt = 1.0/(resolution);
    guide g;
    for (int i=0; i<resolution+1; ++i) {
      real t = i*dt;
      if (dir==0) {
         g = g .. this.gridPoint(x, t);
      } else {
         g = g .. this.gridPoint(t, x);
      }
    }
    draw(g, p=p);
  }
  void drawEtaLine(real eta, pen p=black, int resolution=10) {
    drawGridLine(1, eta, p=p, resolution=resolution);
  }  
  void drawXiLine(real xi, pen p=black, int resolution=10) {
    drawGridLine(0, xi, p=p, resolution=resolution);
  }
  void drawGrid(int nx, int ny, pen p=black, int resolution=10) {
    for (real xi : uniform(0.0, 1.0, nx)) {
      this.drawXiLine(xi, p=p, resolution=resolution);
    }
    for (real eta : uniform(0.0, 1.0, ny)) {
      this.drawEtaLine(eta, p=p, resolution=resolution);
    }  
  }
}

struct TwinBlob {
  restricted Blob[] blobs = new Blob[2];
  restricted pair[] corners;
  restricted real[] corner_angles;
  restricted real[] corner_twist;
  restricted pair[] side_factor;
  restricted path outline;
  restricted pair[] blob_displacement = {(0,0),(0,0)};
  restricted real scale = 1.0;
  restricted int[][] blob_corners = {{0,4,5,3},{4,1,2,5}};

  void operator init(pair[] corners = new pair[] {(0,0), (2,0), (2,1), (0,1), (1,0), (1,1)},
                     real[] corner_angles = new real[] {90,90,90,90,90,90},
                     real[] corner_twist = new real[] {0,0,0,0,0,0},
                     real[] side_factor=new real[] {1,1,1,1,1,1}, real scale=1.0) {
    this.blobs[0] = Blob();
    this.blobs[1] = Blob();
    this.corners = corners;
    for (int i=0; i<6; ++i) {
      this.corners[i] = scale*this.corners[i];
    }
    this.corner_angles = corner_angles;
    this.corner_twist = corner_twist;
    this.side_factor = side_factor;
    this.scale = scale;
  }
  void update() {
    for(int i=0; i<2; ++i) {
      for(int j=0; j<4; ++j) {
        this.blobs[i].corners[j] = this.corners[this.blob_corners[i][j]];
        this.blobs[i].corner_twist[j] = this.corner_twist[this.blob_corners[i][j]];
        this.blobs[i].corner_angles[j] = this.corner_angles[this.blob_corners[i][j]];
      }
      this.blobs[i].move(this.blob_displacement[i]);
      this.blobs[i].update();
    }
    this.outline = this.blobs[0].sides[3]..this.blobs[0].sides[0]..this.blobs[1].sides[0];
    this.outline = this.outline..this.blobs[1].sides[1]..this.blobs[1].sides[2]..this.blobs[0].sides[3];

    this.outline = this.blobs[0].sides[0]..this.blobs[1].sides[0]..this.blobs[1].sides[1]..this.blobs[1].sides[2]..this.blobs[0].sides[2]..this.blobs[0].sides[3]..cycle;
  }
  void draw() {
    this.update();
    this.blobs[0].draw(yellow);
    this.blobs[1].draw(cyan);
  }  
  void setTwist(int at, real angle) {
    this.corner_twist[at] = angle;
  }
  void setTwist(real[] angle) {
    this.corner_twist = angle;
  }
  void setCorner(int at, pair point) {
    this.corners[at] = this.scale*point;
  }
  void moveCorner(int at, pair displacement) {
    this.corners[at] += displacement;
  }
  void setAngle(real[] angle) {
    for (int i=0; i<4; ++i) {
       this.corner_angles[i] = angle[i];
    }
  }
  void setPartDisplacement(int index, pair displacement) {
    this.blob_displacement[index] = displacement;
  }
  void drawXiLine(int index, real xi, pen p=black) {
     this.blobs[index].drawXiLine(xi, p=p);
  }
  void drawGrid(int index, int nx, int ny, pen p=black, int resolution=10) {
     this.blobs[index].drawGrid(nx, ny, p=p, resolution=resolution);
  }
  void drawLoad(real start, real stop) {
     real b = reltime(this.outline, start);
     real e = reltime(this.outline, stop);
     path p = subpath(this.outline, b, e);
     draw(offset(p, 0.5));
  }

  guide getOutlineOffset(real distance, real start=0.0, real stop=1.0, int resolution=10) {
     guide g;
     real dt = 1.0/resolution;
     int[] b = {0, 1, 1, 1, 0, 0};
     int[] c = {0, 0, 1, 2, 2, 3};
     int[] s = {0, 0, 1, 2, 2, 3};
     for (int j=0; j<6; ++j) {
        for (int i=1; i<resolution; ++i) {
          real t = dt*i;
          g = g .. (this.blobs[b[j]].getSidePoint(s[j], t)+distance*this.blobs[b[j]].getSideNormal(s[j],t));
        }
     }
     g = g .. cycle;
     real btime = reltime(g, start);
     real etime = reltime(g, stop);
     return  subpath(g, btime, etime);
   }

   path[] getBoundaryRibbon(real distance, real start=0.0, real stop=1.0, int resolution=10) {
      path outer = this.getOutlineOffset(distance, start=start, stop=stop, resolution=resolution);

      real projectedTime(real rtime) {
         real alpha = sgn(distance)*90;
         pair spA = relpoint(outer, rtime);
         pair normal = unit(rotate(alpha)*dir(outer, reltime(outer, rtime)));
         pair spB = spA + normal*distance*1.2;
         real[] rt = intersect(this.outline, spA--spB);
         if (rt.length < 1) {
           return 0.0;
         }
         return rt[0];
      }
      real st = projectedTime(0.0);
      real et = projectedTime(1.0);
      path inner = reverse(subpath(this.outline, st, et));
      path[] borders = {outer, inner};
      return borders;
   }
   void drawKinematicConstraint(real distance, real start=0.0, real stop=1.0, int resolution=10) {
      path[] borders  = this.getBoundaryRibbon(distance, start, stop, resolution);
      path contour = borders[0]--borders[1]--cycle;
      draw(contour, p=blue+1);
      fill(contour, pattern('hatch'));
   }
   void drawNormalLoad(real distance, real start=0.0, real stop=1.0, int resolution=10, int arrowsDensity=10,
                      int loadsign=1) {
      path[] borders  = this.getBoundaryRibbon(distance, start, stop, resolution);
      path[] normals = getNormals(borders[0], distance, arrowsDensity);
      for (int i=0; i<normals.length; ++i) {
         draw(normals[i], arrow=Arrow);
      }
      draw(borders[0], p=red+2);
   }
}

/*
struct GridBlob {
  restricted Blob[][] blobs;
  restricted pair[][] corners;
  restricted real[][] corner_angles;
  restricted real[][] corner_twist;
  restricted pair[][] side_factor;
  restricted path outline;
  restricted pair[] blob_displacement = {(0,0),(0,0)};
  restricted real scale = 1.0;
  restricted int[][] blob_corners = {{0,4,5,3},{4,1,2,5}};

  void operator init(pair[] corners = new pair[] {(0,0), (2,0), (2,1), (0,1), (1,0), (1,1)},
                     real[] corner_angles = new real[] {90,90,90,90,90,90},
                     real[] corner_twist = new real[] {0,0,0,0,0,0},
                     real[] side_factor=new real[] {1,1,1,1,1,1}, real scale=1.0) {
    this.blobs[0] = Blob();
    this.blobs[1] = Blob();
    this.corners = corners;
    for (int i=0; i<6; ++i) {
      this.corners[i] = scale*this.corners[i];
    }
    this.corner_angles = corner_angles;
    this.corner_twist = corner_twist;
    this.side_factor = side_factor;
    this.scale = scale;
  }
  void update() {
    for(int i=0; i<2; ++i) {
      for(int j=0; j<4; ++j) {
        this.blobs[i].corners[j] = this.corners[this.blob_corners[i][j]];
        this.blobs[i].corner_twist[j] = this.corner_twist[this.blob_corners[i][j]];
        this.blobs[i].corner_angles[j] = this.corner_angles[this.blob_corners[i][j]];
      }
      this.blobs[i].move(this.blob_displacement[i]);
      this.blobs[i].update();
    }
    this.outline = this.blobs[0].sides[3]..this.blobs[0].sides[0]..this.blobs[1].sides[0];
    this.outline = this.outline..this.blobs[1].sides[1]..this.blobs[1].sides[2]..this.blobs[0].sides[3];

    this.outline = this.blobs[0].sides[0]..this.blobs[1].sides[0]..this.blobs[1].sides[1]..this.blobs[1].sides[2]..this.blobs[0].sides[2]..this.blobs[0].sides[3]..cycle;
  }
  void draw() {
    this.update();
    this.blobs[0].draw(yellow);
    this.blobs[1].draw(cyan);
  }
  void setTwist(int at, real angle) {
    this.corner_twist[at] = angle;
  }
  void setTwist(real[] angle) {
    this.corner_twist = angle;
  }
  void setCorner(int at, pair point) {
    this.corners[at] = this.scale*point;
  }
  void moveCorner(int at, pair displacement) {
    this.corners[at] += displacement;
  }
  void setAngle(real[] angle) {
    for (int i=0; i<4; ++i) {
       this.corner_angles[i] = angle[i];
    }
  }
  void setPartDisplacement(int index, pair displacement) {
    this.blob_displacement[index] = displacement;
  }
  void drawXiLine(int index, real xi, pen p=black) {
     this.blobs[index].drawXiLine(xi, p=p);
  }
  void drawGrid(int index, int nx, int ny, pen p=black, int resolution=10) {
     this.blobs[index].drawGrid(nx, ny, p=p, resolution=resolution);
  }
  void drawLoad(real start, real stop) {
     real b = reltime(this.outline, start);
     real e = reltime(this.outline, stop);
     path p = subpath(this.outline, b, e);
     draw(offset(p, 0.5));
  }

  guide getOutlineOffset(real distance, real start=0.0, real stop=1.0, int resolution=10) {
     guide g;
     real dt = 1.0/resolution;
     int[] b = {0, 1, 1, 1, 0, 0};
     int[] c = {0, 0, 1, 2, 2, 3};
     int[] s = {0, 0, 1, 2, 2, 3};
     for (int j=0; j<6; ++j) {
        for (int i=1; i<resolution; ++i) {
          real t = dt*i;
          g = g .. (this.blobs[b[j]].getSidePoint(s[j], t)+distance*this.blobs[b[j]].getSideNormal(s[j],t));
        }
     }
     g = g .. cycle;
     real btime = reltime(g, start);
     real etime = reltime(g, stop);
     return  subpath(g, btime, etime);
   }

   path[] getBoundaryRibbon(real distance, real start=0.0, real stop=1.0, int resolution=10) {
      path outer = this.getOutlineOffset(distance, start=start, stop=stop, resolution=resolution);

      real projectedTime(real rtime) {
         real alpha = sgn(distance)*90;
         pair spA = relpoint(outer, rtime);
         pair normal = unit(rotate(alpha)*dir(outer, reltime(outer, rtime)));
         pair spB = spA + normal*distance*1.2;
         real[] rt = intersect(this.outline, spA--spB);
         if (rt.length < 1) {
           return 0.0;
         }
         return rt[0];
      }
      real st = projectedTime(0.0);
      real et = projectedTime(1.0);
      path inner = reverse(subpath(this.outline, st, et));
      path[] borders = {outer, inner};
      return borders;
   }
   void drawKinematicConstraint(real distance, real start=0.0, real stop=1.0, int resolution=10) {
      path[] borders  = this.getBoundaryRibbon(distance, start, stop, resolution);
      path contour = borders[0]--borders[1]--cycle;
      draw(contour, p=blue+1);
      fill(contour, pattern('hatch'));
   }
   void drawNormalLoad(real distance, real start=0.0, real stop=1.0, int resolution=10, int arrowsDensity=10,
                      int loadsign=1) {
      path[] borders  = this.getBoundaryRibbon(distance, start, stop, resolution);
      path[] normals = getNormals(borders[0], distance, arrowsDensity);
      for (int i=0; i<normals.length; ++i) {
         draw(normals[i], arrow=Arrow);
      }
      draw(borders[0], p=red+2);
   }
}
*/
