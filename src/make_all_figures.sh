#! bash
echo '\section{Figures}' > all_figures.tex
for file in $@
do
    echo -n "running $file .. "
    echo asy --noView -f pdf $file
    if [ $? -eq 0 ]
    then
      echo "OK"
    else
      echo "FAILURE"
      exit 22
    fi
    cat >> all_figures.tex <<-'EOH'
\begin{figure}[hbt]
  \begin{center}
EOH
    echo '\includegraphics{'`basename $file .pdf`'}' >> all_figures.tex
    noufile=`echo $file | sed -e 's/_/\\\\_/g'`
    asyfile=`basename $file .pdf`.asy
    nouasyfile=`echo $asyfile | sed -e 's/_/\\\\_/g'`
    cat >> all_figures.tex <<-EOH
\\end{center}
\\caption{File: $noufile }
\\end{figure}
EOH
cat >> all_figures.tex <<-EOH
\\lstinputlisting[caption=$nouasyfile]{$asyfile}
EOH
done    
