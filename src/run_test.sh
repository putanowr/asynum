#! bash
for file in $@
do
    echo -n "running $file .. "
    asy --noView -f pdf $file
    if [ $? -eq 0 ]
    then
      echo "OK"
    else
      echo "FAILURE"
      exit 22
    fi
done    
